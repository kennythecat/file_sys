# HW01 Implement System Call

## Table of Contents
- [HW01 Implement System Call](#hw01-implement-system-call)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Prerequest](#prerequest)
  - [Part 1](#part-1)
  - [Part 2](#part-2)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description

1. NachOS​
- A process runs on top of another OS​
- kernel (OS) and MIPS code machine simulator​
2. Goal of this Project​
- Understand how to work on Linux machine​
- Understand how system call are done by OS​
3. Go here for more information of NachOS​
- https://www.youtube.com/watch?veLQU3zlpoRQ​

  <img src="./nachos.png"  alt="System Call" width="400" height="300" title="System Call">
## Prerequest
You can choose others OS you like, and also feel free to use Wsl or docker,
here i choosing CentOs7 and using VMware.

https://ftp.tc.edu.tw/Linux/CentOS/7.9.2009/isos/x86_64/ (CentOs 7)
https://www.vmware.com/tw/products/workstation-player/workstation-player-evaluation.html (VMware)
## Part 1 
Setup NachOS 
> 1. Download & Extraction NachOs
> https://drive.google.com/file/d/1Aispn84ruOJ8clYNPfEiEp4YLYb23BrM/edit
> 2. Prepare your environment
> ```
>   cd to unzipped folder​
>   sudo mv ./mips-decstation.linux-xgcc.gz /​
>   cd /​
>   tar -xvzf mips-decstation.linux-xgcc.gz
> ```
> 3. Install packages for compiling​
> ```
>   sudo yum install compat-gcc-44 compat-gcc-44-c++
>   sudo yum install gcc gcc-c++
>   sudo yum install glibc-devel.i686 libgcc.i686 libstdc++-devel.i686 libstdc++-devel
> ```
> 4. Compile
> ```
>   cd NachOS-4.0_MP1
>   chmod -R 777 *
>   cd code/build.linux
>   make clean
>   make​
>```
> 5. Test your nachos by Halt() systemcall
>```
>   cd NachOS-4.0_MP1/code/test
>   make clean
>   make halt​
>   ../build.linux/nachos -e halt
> ```
> 6.  Result will look like this:
> ```
>   halt
>   Machine halting!
>
>   This is halt
>   Ticks: total 52, idle 0, system 40, user 12
>   Disk I/O: reads 0, writes 0
>   Console I/O: reads 0, writes 0
>   Paging: faults 0
>   Network I/O: packets received 0, sent 0
> ```
> 

## Part 2
Implement System Calls ​for File System Operation
> 1. Implement a console I/O system call 
> Void PrintInt(int number)
> 
> 2. Implement four file I/O system call
>     1. OpenFileId Open(char *name)
>     2. int Write(char *buffer, int size, OpenFileId id);
>     3. int Read(char *buffer, int size, OpenFileId id);​  
>     4. int Close(OpenFileId id);​

## Rivision history : v1.0


