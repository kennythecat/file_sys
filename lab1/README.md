# Lab1 Implement FTL Read/Write

## Table of Contents
- [Lab1 Implement FTL Read/Write](#lab1-implement-ftl-readwrite)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Prerequest](#prerequest)
  - [Demo](#demo)
  - [Challenge 1](#challenge-1)
  - [Challenge 2](#challenge-2)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description
Learning Flash Transaction Layer (FTL) algorithm in SSD device by FUSE kernel function.
1. Background
   - Real storage is NAND_0~NAND_X
      - It always read/write 512B data
      - If data is less than 512B, it still stores 512B
      - If data is larger than 512B, it shall store at different NAND
      - Each NAND_X is 1MB
   - FUSE_SSD will represent as FS (/tmp/fuse/SSD)
      - /tmp/fuse/SSD is a file
      - /tmp/fuse/SSD max size = total NAND_X size
      - FUSE_SSD will split data into 512B chunk size and store at different NANDs
2. LBA requirement
   - Handle Logical and Physical address mapping
   - #ls will show logical size (512B align) not physical size
   - EX.
      - User write 256B offset_0 10 times
      - Logical Shall be size 512B
      - Physical shall be 512B*10
<img src="./overview.png"  alt="overview" width="400" height="300" title="overview">

## Prerequest
1. Install Source(fuse3 only support after ubuntu20)
   ```
   apt-cache search fuse
   sudo apt-get update
   sudo apt-get install fuse3
   sudo apt-get install libfuse3-dev
   sudo apt-get install pkg-config
   sudo apt-get install libopencv-dev
   ```
2. Modify NAND_LOCATION to file location
   [ssd_fuse_header.h](ssd_fuse_lab/ssd_fuse_header.h)
   ```
   #define NAND_LOCATION  "here_modified_to_yours_pwd" 
   ```
3. Compile ssd_fuse.c and ssd_fuse_dut.c
   [make_ssd](ssd_fuse_lab/make_ssd)
   ```
   ./make_ssd
   ```
4.  Start SSD fuse lib with debug mode enable
      1. Create dir 
         ```mkdir /tmp/ssd```
      2. Mount it
         ```./ssd_fuse -d /tmp/ssd```
         Console will look like:
         ```
         FUSE library version: 3.10.5
         nullpath_ok: 0
         unique: 2, opcode: INIT (26), nodeid: 0, insize: 56, pid: 0
         INIT: 7.32
         flags=0x03fffffb
         max_readahead=0x00020000
            INIT: 7.31
            flags=0x0040f039
            max_readahead=0x00020000
            max_write=0x00100000
            max_background=0
            congestion_threshold=0
            time_gran=1
            unique: 2, success, outsize: 80
         unique: 4, opcode: GETATTR (3), nodeid: 1, insize: 56, pid: 137
         getattr[NULL] /
            unique: 4, success, outsize: 120
         ...
         ...
         ```

## Demo
1. Check file's detail
   
   ```ls -al /tmp/ssd/ssd_file```

   output:
   ```-rw-r--r-- 1 ubuntu ubuntu 0 Sep  4 18:42 /tmp/ssd/ssd_file```

   write a data to it:

   ```echo "test123" > /tmp/ssd/ssd_file```

   size will increase (0->8):

   ```-rw-r--r-- 1 ubuntu ubuntu 8 Sep  4 18:46 /tmp/ssd/ssd_file```

   The fuse ssd will print out it allocate a pca to store data:
   ```
   write[0] 8 bytes to 0 flags: 0x8001
      write[0] 8 bytes to 0
      unique: 28, success, outsize: 24
   unique: 30, opcode: RELEASE (18), nodeid: 2, insize: 64, pid: 0
      unique: 30, success, outsize: 16
   unique: 32, opcode: LOOKUP (1), nodeid: 1, insize: 49, pid: 198
   LOOKUP /ssd_file
   getattr[NULL] /ssd_file
      NODEID: 2
      unique: 32, success, outsize: 144
   ```
2. Although logical size is 12B, the physical size is actually 512B.
   
   ```./ssd_fuse_dut /tmp/ssd/ssd_file l```

   output : ```8```

   ```./ssd_fuse_dut /tmp/ssd/ssd_file p```

   output : ```1``` 

## Challenge 1
Implement basic write/read function: ftl_read, ftl_write, ssd_do_read and ssd_do_write

## Challenge 2
 Implement host to do write/read compare

## Rivision history : v1.0




