# Lab2 Implement Garbage Collection

## Table of Contents
- [Lab2 Implement Garbage Collection](#lab2-implement-garbage-collection)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Prerequest](#prerequest)
  - [Demo](#demo)
  - [Challenge 1](#challenge-1)
  - [Challenge 2](#challenge-2)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description
Group with the most efficient gc algorithm (smallest WAF) can win.
1. What is GC and why do we need to do GC?   
   - Host continues to write large chunk size data…
   - Open block becomes static when full
   - Some physical page become invalid
  <img src="./gc1.jpg" width="350" height="400" >

2. What if host keeps writing until empty block runs out?
  <img src="./gc2.jpg" width="350" height="400" >

3. Before empty block runs out, we should do Garbage Collection (GC)
   - We collect valid data to new block and reclaim blocks filled with invalid data
   - So that we can erase the reclaimed blocks and use them for new data
  <img src="./gc3.jpg" width="300" height="400" >
  
Some vocabulary:
Write Amplification Factor (WAF) = Data written to NAND Flash / Data written by host
Terabytes Written (TBW) = (User Capacity (GB) × NAND P/E Cycles) / (WAF × 1024)
Over-Provision = (Physical capacity – User capacity) / User capacity

## Prerequest
[Lab1](../lab1)
## Demo


## Challenge 1
Modify writing sequence to Sequence B
<img src="./Sequence.png">

## Challenge 2
Implement gc function (Our Group WAF = 1.2)

## Rivision history : v1.0



