#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include "ssd_fuse_header.h"
#include <time.h>
#define PAGE_SIZE 512
#define PAGE_PER_BLOCK (NAND_SIZE_KB*2)
// Size constants
#define WRITE_SIZE 4096 // 4KB
#define NEARLY_FULL (LOGICAL_NAND_NUM * NAND_SIZE_KB * 1024) - WRITE_SIZE //5*10*1024-4096
#define SIGNIFICANT_PORTION (NEARLY_FULL / 2)

size_t get_physical_size(const char *path) {
    size_t size;
    int fd = open(path, O_RDWR);
    if (fd < 0) {
        perror("Failed to open SSD file");
        return (size_t)-1;
    }
    if (ioctl(fd, SSD_GET_PHYSIC_SIZE, &size) != 0) {
        perror("Failed to get physical size");
        close(fd);
        return (size_t)-1;
    }
    close(fd);
    return size;
}

static int do_rw(FILE* fd, int is_read, size_t size, off_t offset)
{
    char* buf;
    int idx;
    ssize_t ret;
    buf = calloc(1, size);

    if (!buf)
    {
        fprintf(stderr, "failed to allocated %zu bytes\n", size);
        return -1;
    }
    if (is_read)
    {
        printf("dut do read size %ld, off %d\n", size, (int)offset);
        fseek( fd, offset, SEEK_SET );
        ret = fread( buf, 1, size, fd);
        if (ret >= 0)
        {
            fwrite(buf, 1, ret, stdout);
        }
    }
    else
    {
        for ( idx = 0; idx < size; idx++)
        {
            buf[idx] = idx;
        }
        printf("dut do write size %ld, off %d\n", size, (int)offset);
        fseek( fd, offset, SEEK_SET );
        printf("fseek \n");
        ret = fwrite(buf, 1, size, fd);
        //arg.size = fread(arg.buf, 1, size, stdin);
        fprintf(stderr, "Writing %zu bytes\n", size);
    }
    if (ret < 0)
    {
        perror("ioctl");
    }

    free(buf);
    return ret;
}

int w(char* path, int size, int off){
    int rc;
    char cmd;
    FILE* fptr;
    if ( !(fptr = fopen(path, "r+")))
    {
        perror("open");
        return 1;
    }
    rc = do_rw(fptr, cmd == 'r', size, off);
    if (rc < 0)
    {
        return 1;
    }
    fprintf(stderr, "transferred %d bytes \n", rc);
    fclose(fptr);
    return 0;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s \n fill the stroage: f\n delete: d", argv[0]);
        return 1;
    }
    char cmd = argv[1][0];
    char *path = "/tmp/ssd/ssd_file";
    char buf[WRITE_SIZE];
    int invalidCnt[LOGICAL_NAND_NUM]={1, 3, 2};
    if(cmd=='f'){
        printf("Filling storage...\n");
        w(path, 3*PAGE_PER_BLOCK*PAGE_SIZE-1, 0);
    }
    else if(cmd=='d'){
        for(int i=0;i<LOGICAL_NAND_NUM;i++){
            for(int j=0;j<invalidCnt[i];j++){
                w(path, 1, i*PAGE_PER_BLOCK*PAGE_SIZE+j*PAGE_SIZE);
            }
        }
    }

    // delete nand0
    
    // w(path, 1, 0);
    // w(path, 1, 1*PAGE_PER_BLOCK*PAGE_SIZE+1*PAGE_SIZE);
    

    size_t physical_size_before = get_physical_size(path);
    if (physical_size_before == (size_t)-1) {
        return 1;
    }
    printf("Physical size before GC: %zu\n", physical_size_before);


    // sleep(2);

    size_t physical_size_after = get_physical_size(path);
    if (physical_size_after == (size_t)-1) {
        return 1;
    }
    printf("Physical size after GC:  %zu\n", physical_size_after);

    if (physical_size_after > physical_size_before) {
        fprintf(stderr, "Garbage Collection didn't reclaim space!\n");
        return 1;
    }

    printf("Garbage collection test passed!\n");
    printf("Make sure to umount the ssd : fusermount -u /tmp/ssd \n");
    return 0;
}
