/*
  FUSE ssd: FUSE ioctl example
  Copyright (C) 2008       SUSE Linux Products GmbH
  Copyright (C) 2008       Tejun Heo <teheo@suse.de>
  This program can be distributed under the terms of the GNU GPLv2.
  See the file COPYING.
*/
#define GC_THRESHOLD 0.6 // Collect garbage if more than 70% of pages in a block are invalid
#define FUSE_USE_VERSION 35
#define PAGES_PER_BLOCK (NAND_SIZE_KB*2)
#include <fuse.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <math.h>
#include "ssd_fuse_header.h"
#define SSD_NAME       "ssd_file"
enum
{
    SSD_NONE,
    SSD_ROOT,
    SSD_FILE,
};
static size_t physic_size;
static size_t logic_size;
static size_t host_write_size;
static size_t nand_write_size;
static int nand_erase(int); 
void garbage_collection(void);

typedef union pca_rule PCA_RULE;
union pca_rule
{
    unsigned int pca;
    struct
    {
        unsigned int lba : 16;
        unsigned int nand: 16;
    } fields;
};

PCA_RULE curr_pca;

typedef union pna_rule PNA_RULE;
union pna_rule
{
    unsigned int pna;
    struct{
        unsigned int rank   : 4;
        unsigned int invalid: 20;
        unsigned int pe     : 8;
    }fields;
};
PNA_RULE curr_pna;

unsigned int* L2P;
PNA_RULE  *IvT;
int* page_validity;

void print_nand_invalid_rate() {
    int pages_per_block = NAND_SIZE_KB * 1024 / 512; // Total pages per NAND block
    printf("NAND Invalid Rate:\n");
    for (int nand = 0; nand < PHYSICAL_NAND_NUM; nand++) {
        int invalid_pages = 0;

        // Calculate and print the invalid rate for this NAND block
        float invalid_rate = (float)IvT[nand].fields.invalid / pages_per_block;
        printf("NAND %d: Invalid pages = %d/%d, Invalid rate = %.2f%%\n", nand, IvT[nand].fields.invalid, pages_per_block, invalid_rate * 100.0);
        // if(invalid_rate>0.3) nand_erase(0);
    }
}

static int ssd_resize(size_t new_size)
{
    //set logic size to new_size
    if (new_size >= LOGICAL_NAND_NUM * NAND_SIZE_KB * 1024  ) //5*10*1024
    {
        return -ENOMEM;
    }
    else
    {
        logic_size = new_size;
        return 0;
    }
}

static int ssd_expand(size_t new_size)
{
    //logic must less logic limit

    if (new_size > logic_size)
    {
        return ssd_resize(new_size);
    }
    return 0;
}

static int nand_read(char* buf, int pca)
{
    char nand_name[100];
    FILE* fptr;

    PCA_RULE my_pca;
    my_pca.pca = pca;
    snprintf(nand_name, 100, "%s/nand_%d", NAND_LOCATION, my_pca.fields.nand);

    //read
    if ( (fptr = fopen(nand_name, "r") ))
    {
        fseek( fptr, my_pca.fields.lba * 512, SEEK_SET );
        fread(buf, 1, 512, fptr);
        fclose(fptr);
    }
    else
    {
        printf("open file fail at nand read pca = %d\n", pca);
        return -EINVAL;
    }
    return 512;
}
static int nand_write(const char* buf, int pca)
{
    printf("---------------nand_write--------------\n");
    char nand_name[100];
    FILE* fptr;

    PCA_RULE my_pca;
    my_pca.pca = pca;
    snprintf(nand_name, 100, "%s/nand_%d", NAND_LOCATION, my_pca.fields.nand);
    //write
    if ( (fptr = fopen(nand_name, "r+")))
    {
        fseek( fptr, my_pca.fields.lba * 512, SEEK_SET );
        fwrite(buf, 1, 512, fptr);
        fclose(fptr);
        IvT[my_pca.fields.nand].fields.pe++;
        physic_size ++;
    }
    else
    {
        printf("open file fail at nand (%s) write pca = %d, return %d\n", nand_name, pca, -EINVAL);
        return -EINVAL;
    }
    nand_write_size += 512;
    return 512;
}

static int nand_erase(int nand) {
    printf("----nand_erase------\n");
    char nand_name[100];
    FILE *fptr;
    int pages_per_block = NAND_SIZE_KB * 1024 / 512;
    snprintf(nand_name, 100, "%s/nand_%d", NAND_LOCATION, nand);
    
    if ((fptr = fopen(nand_name, "w"))) {
        fclose(fptr);
        if(physic_size >= pages_per_block){
            physic_size -= pages_per_block;
        }
        else physic_size=0;

        // Update L2P mapping for the erased NAND block
        for (int lba = 0; lba < pages_per_block; lba++) {
            int logical_page = nand * pages_per_block + lba;
            L2P[logical_page] = VALID_PCA; // Assuming VALID_PCA represents a valid state
        }
        IvT[nand].fields.invalid = 0;
        printf("NAND erase %d pass, new physical size: %zu\n", nand, physic_size);
        return 1;
    } else {
        printf("open file fail at nand (%s) erase nand = %d, return %d\n", nand_name, nand, -EINVAL);
        return -EINVAL;
    }
}
static unsigned int get_next_pca()
{
    printf("NOW PCA = lba %d, nand %d\n", curr_pca.fields.lba, curr_pca.fields.nand);
    if (curr_pca.pca == VALID_PCA)
    {
        curr_pca.pca = 0;
        return curr_pca.pca;
    }
    else if (curr_pca.pca == FULL_PCA)
    {
        printf("No new PCA\n");
        return FULL_PCA;
    }
    // if (curr_pca.fields.nand >= PHYSICAL_NAND_NUM - (0.1 * PHYSICAL_NAND_NUM))  // If less than 10% blocks are left
    // if(curr_pca.fields.nand >= LOGICAL_NAND_NUM)
    // {
    //     garbage_collection();
    // }

    if (curr_pca.fields.lba == (NAND_SIZE_KB * 1024 / 512) - 1) //10kb/512b  ~ 20
    {
        curr_pca.fields.nand += 1;
    }
    curr_pca.fields.lba = (curr_pca.fields.lba + 1) % PAGES_PER_BLOCK;

    if (curr_pca.fields.nand >= PHYSICAL_NAND_NUM - 1)
    {
        printf("No new PCA\n");
        curr_pca.pca = FULL_PCA;
        return FULL_PCA;
    }
    else
    {
        printf("Next PCA = lba %d, nand %d\n", curr_pca.fields.lba, curr_pca.fields.nand);
        return curr_pca.pca;
    }
}


static int ftl_read(char* buf, size_t lba)
{
    unsigned int pca;
    pca = L2P[lba];

    // If the LBA hasn't been written to yet or is invalid, return an error or zero-filled data
    if (pca == INVALID_PCA)
    {
        // return -EINVAL;
        memset(buf, 0, 512);
        return 512;
    }

    // Read data from the NAND using the translated PCA
    return nand_read(buf, pca);
}

static int ftl_write(const char* buf, size_t lba)
{
    printf("---------------------ftl_write-----------------------\n");
    int pca = get_next_pca();
    if (pca == FULL_PCA) {
        return -ENOMEM;
    }
    if (nand_write(buf, pca) < 0) {
        return -EIO; 
    }
    L2P[lba] = pca;
    PCA_RULE my_pca;
    my_pca.pca = pca;
    printf("Wrote to LBA %zu, new physical size: %zu, pca.lba: %zu\n", lba, physic_size, my_pca.fields.lba);
    return pca;
}


static int ssd_file_type(const char* path)
{
    if (strcmp(path, "/") == 0)
    {
        return SSD_ROOT;
    }
    if (strcmp(path, "/" SSD_NAME) == 0)
    {
        return SSD_FILE;
    }
    return SSD_NONE;
}
static int ssd_getattr(const char* path, struct stat* stbuf,
                       struct fuse_file_info* fi)
{
    (void) fi;
    stbuf->st_uid = getuid();
    stbuf->st_gid = getgid();
    stbuf->st_atime = stbuf->st_mtime = time(NULL);
    switch (ssd_file_type(path))
    {
        case SSD_ROOT:
            stbuf->st_mode = S_IFDIR | 0755;
            stbuf->st_nlink = 2;
            break;
        case SSD_FILE:
            stbuf->st_mode = S_IFREG | 0644;
            stbuf->st_nlink = 1;
            stbuf->st_size = logic_size;
            break;
        case SSD_NONE:
            return -ENOENT;
    }
    return 0;
}
static int ssd_open(const char* path, struct fuse_file_info* fi)
{
    (void) fi;
    if (ssd_file_type(path) != SSD_NONE)
    {
        return 0;
    }
    return -ENOENT;
}
static int ssd_do_read(char* buf, size_t size, off_t offset)
{
    int tmp_lba, tmp_lba_range, rst;
    char* tmp_buf;

    // off limit
    if ((offset) >= logic_size)
    {
        return 0;
    }
    if (size > logic_size - offset)
    {
        //is valid data section
        size = logic_size - offset;
    }

    tmp_lba = offset / 512;
    tmp_lba_range = (offset + size - 1) / 512 - tmp_lba + 1;
    tmp_buf = calloc(tmp_lba_range * 512, sizeof(char));

    for (int i = 0; i < tmp_lba_range; i++)
    {
        rst = ftl_read(tmp_buf + i * 512, tmp_lba + i);
        if (rst < 0)
        {
            free(tmp_buf);
            return rst; // If read fails, free buffer and return the error
        }
    }

    memcpy(buf, tmp_buf + offset % 512, size);
    free(tmp_buf);

    return size;
}
static int ssd_read(const char* path, char* buf, size_t size,
                    off_t offset, struct fuse_file_info* fi)
{
    (void) fi;
    if (ssd_file_type(path) != SSD_FILE)
    {
        return -EINVAL;
    }
    return ssd_do_read(buf, size, offset);
}
static int ssd_do_write(const char* buf, size_t size, off_t offset)
{
    printf("---------------------ssd_do_write-------------------------\n");
    // size_t prev_physic_size = physic_size;
    int tmp_lba, tmp_lba_range, process_size;
    int idx, remain_size;
    char *rmw_buffer = calloc(512, sizeof(char));

    host_write_size += size;
    if (ssd_expand(offset + size) != 0)
    {
        return -ENOMEM;
    }

    tmp_lba = offset / 512;
	tmp_lba_range = (offset + size - 1) / 512 - tmp_lba + 1;

    process_size = 0;
    remain_size = size;

    for (idx = 0; idx < tmp_lba_range; idx++)
    {
        size_t start_offset = idx?0:(offset%512); 
        size_t end_offset;
        
        if (idx == tmp_lba_range - 1) {
            end_offset = (offset + size) % 512; 
            if (end_offset == 0) // If the data ends perfectly at a block boundary
                end_offset = 511;
        } else {
            end_offset = 511; // For the first and middle blocks, write till the end
        }
        size_t chunk_size = end_offset - start_offset;

        if (start_offset != 0 || end_offset != 511) {
            // RMW operation
            ftl_read(rmw_buffer, tmp_lba + idx);
            memcpy(rmw_buffer + start_offset, buf + process_size, chunk_size);
            ftl_write(rmw_buffer, tmp_lba + idx);
        } else {
            ftl_write(buf + process_size, tmp_lba + idx);
        }
        process_size += chunk_size;
        remain_size -= chunk_size;
    }
    free(rmw_buffer);

    // size_t delta_physic_size = physic_size - prev_physic_size;
    // if (delta_physic_size > GC_THRESHOLD)  // Determine an appropriate threshold
    // {
    //     garbage_collection();
    // }

    return size;
}
static int ssd_do_delete(char* buf, size_t size, off_t offset)
{
    printf("------------ssd_do_delete----------\n");
    int lba, nand_num;
    lba = offset / 512;
    PCA_RULE my_pca;
    
    // Check if the LBA is within the logical size limits
    if (lba >= LOGICAL_NAND_NUM * NAND_SIZE_KB * 1024 / 512) {
        printf("Error: LBA %zu is out of bounds.\n", lba);
        return -EINVAL;
    }

    // Mark the page as invalid
    my_pca.pca = L2P[lba];
    nand_num=my_pca.fields.nand;
    if (my_pca.pca != INVALID_PCA) {
        IvT[nand_num].fields.invalid++;
        L2P[lba] = INVALID_PCA;
        printf("NAND :%zu, LBA %zu marked as invalid.\n", nand_num, lba);
    } else {
        printf("Page at LBA %zu is already invalid.\n", lba);
    }
    printf("------------ssd_done_delete----------\n");
    print_nand_invalid_rate();
    return 0;
}
static int ssd_write(const char* path, const char* buf, size_t size,
                     off_t offset, struct fuse_file_info* fi)
{
    printf("ssd_write, size=%d, offset=%d\n", size, offset);
    (void) fi;
    if (ssd_file_type(path) != SSD_FILE)
    {
        return -EINVAL;
    }
    if(size==1){
        return ssd_do_delete(buf, size, offset);
    }
    if(size==2){
        garbage_collection();
        return 0;
    }
    else{
        return ssd_do_write(buf, size, offset);
    }
}


static int ssd_truncate(const char* path, off_t size,
                        struct fuse_file_info* fi)
{
    (void) fi;
    if (ssd_file_type(path) != SSD_FILE)
    {
        return -EINVAL;
    }

    return ssd_resize(size);
}
void swap(int *a, int *b) {
    int t = *a;
    *a = *b;
    *b = t;
}
int partition(int arr[], PNA_RULE IvT[], int low, int high) {
    int pivot = IvT[arr[high]].fields.invalid; 
    int i = (low - 1);

    for (int j = low; j <= high - 1; j++) {
        if (IvT[arr[j]].fields.invalid > pivot) {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}
void quickSort(int arr[], PNA_RULE IvT[], int low, int high) {
    if (low < high) {
        int pi = partition(arr, IvT, low, high);

        quickSort(arr, IvT, low, pi - 1);
        quickSort(arr, IvT, pi + 1, high);
    }
}

void garbage_collection() {
    printf("------------------Start GC--------------------------------\n");

    int indices[LOGICAL_NAND_NUM];
    int min=0, max=0;
    int tmp_pca;
    for (int i = 0; i < LOGICAL_NAND_NUM; i++) {
        indices[i] = i;
    }

    quickSort(indices, IvT, 0, LOGICAL_NAND_NUM - 1);

    for (int i = 0; i < LOGICAL_NAND_NUM; i++) {
        IvT[indices[i]].fields.rank = LOGICAL_NAND_NUM-i-1;
    }
    // min = indices[0];
    // max = 
    
    for (int i = 0; i < LOGICAL_NAND_NUM; i++) {
        if (IvT[i].fields.rank && IvT[i].fields.rank < IvT[min].fields.rank) {
            min = i; 
        }
        if (IvT[i].fields.invalid != PAGES_PER_BLOCK && IvT[i].fields.rank > IvT[max].fields.rank) {
            max = i; 
        }
        printf("IvT[%d].fields.invalid = %d; IvT[%d].fields.rank = %d;\n",
               i, IvT[i].fields.invalid, i, IvT[i].fields.rank);
        
    }
    printf("min= %d, max= %d\n", min, max);
    int i=0,j=0;
    while(IvT[min].fields.invalid>0 && IvT[max].fields.invalid<PAGES_PER_BLOCK){
        printf("i=%d, j=%d\n",i ,j);
        while (i < PAGES_PER_BLOCK && L2P[max * PAGES_PER_BLOCK-1 + i] != INVALID_PCA) i++;
        while (j < PAGES_PER_BLOCK && L2P[min * PAGES_PER_BLOCK-1 + j] != INVALID_PCA) j++;
        if (i >= PAGES_PER_BLOCK || j >= PAGES_PER_BLOCK){
            printf("i or j out of block size\n");
            break;
        } 
        tmp_pca = L2P[min * PAGES_PER_BLOCK + j];
        L2P[min * PAGES_PER_BLOCK + j] = L2P[max * PAGES_PER_BLOCK + i];
        L2P[max * PAGES_PER_BLOCK + i] = tmp_pca;

        IvT[min].fields.invalid--;
        IvT[max].fields.invalid++;
        
        i++; j++;
    }
    print_nand_invalid_rate();
}

static int ssd_readdir(const char* path, void* buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info* fi,
                       enum fuse_readdir_flags flags)
{
    (void) fi;
    (void) offset;
    (void) flags;
    if (ssd_file_type(path) != SSD_ROOT)
    {
        return -ENOENT;
    }
    filler(buf, ".", NULL, 0, 0);
    filler(buf, "..", NULL, 0, 0);
    filler(buf, SSD_NAME, NULL, 0, 0);
    return 0;
}
static int ssd_ioctl(const char* path, unsigned int cmd, void* arg,
                     struct fuse_file_info* fi, unsigned int flags, void* data)
{

    if (ssd_file_type(path) != SSD_FILE)
    {
        return -EINVAL;
    }
    if (flags & FUSE_IOCTL_COMPAT)
    {
        return -ENOSYS;
    }
    switch (cmd)
    {
        case SSD_GET_LOGIC_SIZE:
            *(size_t*)data = logic_size;
            return 0;
        case SSD_GET_PHYSIC_SIZE:
            *(size_t*)data = physic_size;
            return 0;
        case SSD_GET_WA:
            *(double*)data = (double)nand_write_size / (double)host_write_size;
            return 0;
    }
    return -EINVAL;
}
static const struct fuse_operations ssd_oper =
{
    .getattr        = ssd_getattr,
    .readdir        = ssd_readdir,
    .truncate       = ssd_truncate,
    .open           = ssd_open,
    .read           = ssd_read,
    .write          = ssd_write,
    .ioctl          = ssd_ioctl,
};
int main(int argc, char* argv[])
{
    int idx;
    char nand_name[100];
    physic_size = 0; // also equal to number of pages
    logic_size = 0;
	nand_write_size = 0;
	host_write_size = 0;
    curr_pca.pca = VALID_PCA;
    L2P = malloc(LOGICAL_NAND_NUM * NAND_SIZE_KB * 1024 / 512 * sizeof(int)); // 16 bit NAND + 16 bit lba = 4byte 
    memset(L2P, VALID_PCA, sizeof(int) * LOGICAL_NAND_NUM * NAND_SIZE_KB * 1024 / 512);
    
    IvT = malloc(PHYSICAL_NAND_NUM * sizeof(int));
    memset(IvT, 0, PHYSICAL_NAND_NUM * sizeof(int)); 
    for(int i = 0; i < PHYSICAL_NAND_NUM;i++){
        IvT[i].fields.rank = i;
    }
    //create nand file
    for (idx = 0; idx < PHYSICAL_NAND_NUM; idx++)
    {   
        printf("%s\n", NAND_LOCATION);  
        FILE* fptr;
        snprintf(nand_name, 100, "%s/nand_%d", NAND_LOCATION, idx);
        fptr = fopen(nand_name, "w");
        if (fptr == NULL)
        {
            printf("open fail");
        }
        fclose(fptr);
    }
    return fuse_main(argc, argv, &ssd_oper, NULL);
}