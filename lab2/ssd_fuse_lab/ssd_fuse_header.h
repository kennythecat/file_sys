/*
  FUSE-ioctl: ioctl support for FUSE
  Copyright (C) 2008       SUSE Linux Products GmbH
  Copyright (C) 2008       Tejun Heo <teheo@suse.de>
  This program can be distributed under the terms of the GNU GPLv2.
  See the file COPYING.
*/
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
//OverProvision = (PHYSICAL_NAND_NUM-LOGICAL_NAND_NUM)/PHYSICAL_NAND_NUM
#define PHYSICAL_NAND_NUM (4) // (8) 
#define LOGICAL_NAND_NUM  (3)  // (5)
#define NAND_SIZE_KB      (2) //   1 nand contain 10 pages <| (10) kb/512b ~ 20
#define VALID_PCA     (0xFFFFFFFF)
#define INVALID_PCA   (0xFFFFFFEE)
#define FULL_PCA      (0xFFFFFFFE)
#define TLC_PE_Cycle  (3000)
#define NAND_LOCATION  "/mnt/c/kennythecat_git/file_sys/lab2/ssd_fuse_lab"

enum
{
    SSD_GET_LOGIC_SIZE    = _IOR('E', 0, size_t),
    SSD_GET_PHYSIC_SIZE   = _IOR('E', 1, size_t),
    SSD_GET_WA            = _IOR('E', 2, size_t),
};
//“IOR“: an ioctl with read parameters (copy_to_user)