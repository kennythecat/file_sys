#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include "ssd_fuse_header.h"

// Size constants
#define WRITE_SIZE 4096 // 4KB
#define NEARLY_FULL (LOGICAL_NAND_NUM * NAND_SIZE_KB * 1024) - WRITE_SIZE //5*10*1024-4096
#define SIGNIFICANT_PORTION (NEARLY_FULL / 2)

// Generates random data for writing
void generate_random_data(char *buf, size_t size) {
    for (size_t i = 0; i < size; i++) {
        buf[i] = rand() % 256;
    }
}

// Benchmark write function
int write_data_to_ssd(const char *path, const char *data, off_t offset) {
    FILE *fptr = fopen(path, "r+");
    if (!fptr) {
        perror("Failed to open SSD file for writing");
        return -1;
    }

    if (fseek(fptr, offset, SEEK_SET) != 0) {
        perror("Failed to seek in SSD file");
        fclose(fptr);
        return -1;
    }

    size_t written = fwrite(data, 1, WRITE_SIZE, fptr);
    if (written != WRITE_SIZE) {
        perror("Failed to write data to SSD");
        fclose(fptr);
        return -1;
    }

    fclose(fptr);
    return 0;
}

// Fetch physical size
size_t get_physical_size(const char *path) {
    size_t size;
    int fd = open(path, O_RDWR);
    if (fd < 0) {
        perror("Failed to open SSD file");
        return (size_t)-1;
    }
    if (ioctl(fd, SSD_GET_PHYSIC_SIZE, &size) != 0) {
        perror("Failed to get physical size");
        close(fd);
        return (size_t)-1;
    }
    close(fd);
    return size;
}

// Main test function
int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s SSD_FILE (ex. /tmp/ssd/ssd_file)\n", argv[0]);
        return 1;
    }

    char *path = argv[1];
    char buf[WRITE_SIZE];
    
    // Step 2: Fill Storage
    printf("Filling storage...\n");
    for (off_t i = 0; i < NEARLY_FULL; i += WRITE_SIZE) {
        generate_random_data(buf, WRITE_SIZE);
        if (write_data_to_ssd(path, buf, i) != 0) {
            return 1;
        }
    }

    size_t physical_size_before = get_physical_size(path);
    if (physical_size_before == (size_t)-1) {
        return 1;
    }
    printf("Physical size before GC: %zu\n", physical_size_before);

    // Step 3: Overwrite Data
    printf("Overwriting data...\n");
    for (off_t i = 0; i < SIGNIFICANT_PORTION; i += WRITE_SIZE) {
        generate_random_data(buf, WRITE_SIZE);
        if (write_data_to_ssd(path, buf, i) != 0) {
            return 1;
        }
    }

    // Step 4: Trigger Garbage Collection 
    // Assuming GC is auto-triggered when writing, this step is implicitly done in Step 3.

    // Give some time or perform some operations that might cause GC to run
    sleep(2);

    size_t physical_size_after = get_physical_size(path);
    if (physical_size_after == (size_t)-1) {
        return 1;
    }
    printf("Physical size after GC:  %zu\n", physical_size_after);

    // Step 5: Verify Space Reclamation
    if (physical_size_after > physical_size_before) {
        fprintf(stderr, "Garbage Collection didn't reclaim space!\n");
        return 1;
    }

    printf("Garbage collection test passed!\n");
    printf("Make sure to umount the ssd : fusermount -u /tmp/ssd \n");
    return 0;
}
