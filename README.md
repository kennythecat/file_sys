# File Store System

## Table of Contents
- [hw00](./hw00) - Trace the system call
- [hw01](./hw01) - Implement System Calls 
- [Phison's Lab - individual](./lab1) - Implement FTL 
- [Phison's Lab - group](./lab2) - Implement Garbege Collectinn 

***

## Description

This repository contains the following assignments and labs :

- `hw00` (2022/2/24)
  
  Trace the source code of the system call - Halt()

- `hw01` (2022/4/13)
  
  Implement System Calls for File System Operation
  1. PrintInt()
  2. Open()
  3. Read()
  4. Write()
  5. Close()
   
- `Phison's Lab - individual` (2022/5/17)
  
  Implement FTL for nand flash
  1. ftl_read
  2. ftl_write 
  3. ssd_do_read 
  4. ssd_do_write
   
- `Phison's Lab - group` (2022/5/21)
  1. Implement Garbege Collectinn for nand flash
  2. Modify writing sequence

## Rivision history : v1.1


