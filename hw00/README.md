# HW00

## Table of Contents
- [HW00](#hw00)
  - [Table of Contents](#table-of-contents)
  - [Part 1](#part-1)
  - [Part 2](#part-2)
  - [Rivision history : v1.0](#rivision-history--v10)
***

## Part 1 
Trace the System Call from NachOs :

- `FlowChart`: 
Trace the source code of ```Halt()``` system call 
![FlowChart of system call](./flowchart.png "FlowChart")

## Part 2
Details of snippet from tracing Code

1. `machine.h`: 
   ```
    void Run();
    ```
2. `mipssim.cc`
   ```
    Machine::Run()
    ...
    kernel->interrupt->setStatus(UserMode);
    for (;;) {
    OneInstruction(instr);
    kernel->interrupt->OneTick();
    if (singleStep && (runUntilTime <= kernel->stats->totalTicks))
    Debugger();
    }
    ...
    ```
3. `mipssim.cc`
   ```
    void Machine::OneInstruction(Instruction *instr)
    ...
    case OP_SYSCALL:
    RaiseException(SyscallException, 0);
    return;

    ```
    
4. `machine.cc`
   ```
    Machine::RaiseException(ExceptionType which, int badVAddr)
    {
    DEBUG(dbgMach, "Exception: " << exceptionNames[which]);
    registers[BadVAddrReg] = badVAddr;
    DelayedLoad(0, 0);
    kernel->interrupt->setStatus(SystemMode);
    ExceptionHandler(which);
    kernel->interrupt->setStatus(UserMode);
    }
    ```
5. `exception.cc`
   ```
   ExceptionHandler(ExceptionType which)
    ...
    switch (which) {
    case SyscallException:
    switch(type) {
    case SC_Halt:
    DEBUG(dbgSys, "Shutdown, initiated by user program.\n");
    SysHalt();
    cout<<"in exception\n";
    ASSERTNOTREACHED();
    break;
    ...
   ```
6. `ksycall.h`
   ```
   void SysHalt(){
    kernel->interrupt->Halt();
    }
   ```
7. `interrupt.cc`
   ```
   voidInterrupt::Halt(){
    delete debug;
    delete kernel;
    }
   ```
8. `kernel.cc`
   ```
   Kernel::~Kernel(){
    delete stats;
    delete interrupt;
    delete scheduler;
    delete alarm;
    delete machine;
    delete synchConsoleIn;
    delete synchConsoleOut;
    delete synchDisk;
    delete fileSystem;
    Exit(0);
    }
   ```
## Rivision history : v1.0




